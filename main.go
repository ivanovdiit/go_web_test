package main

import (
	"fmt"
	"net/http"
	"github.com/go-redis/redis"
	"strconv"
)

var redis_client *redis.Client;

func RedisTest(w http.ResponseWriter, path string) {

	incr := redis_client.Incr("incr")

	fmt.Println(fmt.Sprint(incr.Val()))
	fmt.Fprintf(w, "test " + strconv.FormatInt(incr.Val(), 10))
}

func handler(w http.ResponseWriter, r *http.Request) {
	RedisTest(w, r.URL.Path[1:])
	//fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func main() {

	redis_client = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
		MaxRetries: 5,
	})

	http.HandleFunc("/", handler)
	http.ListenAndServe(":8089", nil)
}